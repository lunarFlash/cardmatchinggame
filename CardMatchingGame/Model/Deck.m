//
//  Deck.m
//  CardMatchingGame
//
//  Created by Yi Wang on 5/11/13.
//  Copyright (c) 2013 Yi. All rights reserved.
//

#import "Deck.h"

@interface Deck ()
//private property for cards in the deck
@property (strong, nonatomic) NSMutableArray *cards;
@end


@implementation Deck

-(NSMutableArray *)cards
{
    //overload getter for lazy instantiation
    if (!_cards){
        _cards = [[NSMutableArray alloc] init];
    }
    return _cards;
}



-(void) addCard:(Card *)card atTop:(BOOL)atTop
{
    if (card){
        if (atTop){
            [self.cards insertObject:card atIndex:0];
        } else{
            [self.cards addObject:card];
        }
    }
}

-(Card *) drawRandomCard
{
    Card *randomCard = nil;
    if (self.cards.count){
        unsigned index = arc4random()%self.cards.count;
        randomCard = self.cards[index];
        [self.cards removeObjectAtIndex:index];
    }
    return randomCard;
}


-(void) printCards
{
    for (Card *currentCard in self.cards){
        NSLog(@"%@", currentCard.contents);
    }
    
}

@end
