//
//  Card.h
//  CardMatchingGame
//
//  Created by Yi Wang on 5/11/13.
//  Copyright (c) 2013 Yi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Card : NSObject
//--Strong means I want this object to stay in the heap as long as I point something to it. weak means only keep it in the heap if someone else points to it.
@property (strong, nonatomic) NSString *contents;

//change name of getter in @ declaration
@property (nonatomic, getter=isFaceUp) BOOL faceUp;
@property (nonatomic) BOOL unplayable;

- (int) match: (NSArray*) otherCards;

@end
