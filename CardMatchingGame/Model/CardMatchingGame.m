//
//  CardMatchingGame.m
//  CardMatchingGame
//
//  Created by Yi Wang on 5/12/13.
//  Copyright (c) 2013 Yi. All rights reserved.
//

#import "CardMatchingGame.h"

@interface CardMatchingGame()

@property (readwrite, nonatomic) int score; //readwrite is by default, we gave a property score which is public readonly and private readwrite.
@property (strong, nonatomic) NSMutableArray *cards; //intended to be of type Card
@end

@implementation CardMatchingGame

//lazy instantiation
-(NSMutableArray *) cards
{
    if (!_cards) _cards = [[NSMutableArray alloc]init];
    return _cards;
}



-(id)initWithCardCount:(NSUInteger)count
             usingDeck:(Deck *)deck
{   //if you are implementing designated initializer, you must call [super init] if you are implementing a convenient initializer, you call [self init]; 
    self = [super init];
    if (self) {
        for (int i = 0; i < count; i++) {
            Card *card = [deck drawRandomCard];
            if (card){  //protect if there are more than 52 cards
                self.cards[i] = card;
            } else {  //if we can't draw cards in the event count > size of a deck, we dont want to create a broken object so we return nil
                self = nil;
                break;
            }
        }
    }
    return self;
}

#define MATCH_BONUS 4
#define MISMATCH_PENALTY 2
#define FLIP_COST 1

-(void)flipCardAtIndex:(NSUInteger)index
{
    Card *card = [self cardAtIndex:index];

    if (card && !card.unplayable) {
        if (!card.isFaceUp){
            for (Card *otherCard in self.cards) {
                if (otherCard.isFaceUp && !otherCard.unplayable){
                    int matchScore = [card match:@[otherCard]]; //@[] means create an NSArray right now
                    if (matchScore){
                        card.unplayable = YES;
                        otherCard.unplayable = YES;
                        self.score += matchScore * MATCH_BONUS;
                    } else{  //they don't match
                        otherCard.faceUp = NO;
                        self.score -= MISMATCH_PENALTY;
                    }
                    break;
                }
            }
            self.score -= FLIP_COST;
        }
        card.faceUp = !card.isFaceUp; //if true, we flip the card
    } 
}


-(Card *)cardAtIndex:(NSUInteger)index
{
    return (index < [self.cards count]) ? self.cards[index]: nil;
}


@end
