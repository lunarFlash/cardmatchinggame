//
//  PlayingCard.h
//  CardMatchingGame
//
//  Created by Yi Wang on 5/11/13.
//  Copyright (c) 2013 Yi. All rights reserved.
//

#import "Card.h"

@interface PlayingCard : Card

@property (strong, nonatomic) NSString *suit;
@property (nonatomic) NSUInteger *rank;

+(NSArray *) validSuits;
+(NSArray *) rankStrings;
+(NSUInteger *) maxRank;
@end
