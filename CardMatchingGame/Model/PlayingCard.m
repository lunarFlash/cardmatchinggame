//
//  PlayingCard.m
//  CardMatchingGame
//
//  Created by Yi Wang on 5/11/13.
//  Copyright (c) 2013 Yi. All rights reserved.
//

#import "PlayingCard.h"

@interface PlayingCard ()

@end

@implementation PlayingCard

@synthesize suit = _suit;


-(int) match:(NSArray *)otherCards
{
    int score = 0;
    // overloading match, we don't call super here because this version of match does a much better job than Card's
    if ([otherCards count] ==1){
        PlayingCard *otherCard = [otherCards lastObject]; //lastObject of NSArray class will never return error
        if ([otherCard.suit isEqualToString:self.suit]){
            score = 1;
        } else if (otherCard.rank == self.rank) {
            score = 4;
        }
    }
    
    return score;
}


//  contents is concatenation of rank and suit rabk is represented as a string by indexig into +rankStrings
-(NSString*) contents
{
    NSArray *rankStrings = [PlayingCard rankStrings];
    int index = self.rank;
    return [rankStrings[index] stringByAppendingString:self.suit];
}

//+ means class utility method used by this class
+(NSArray *)validSuits
{
    return @[@"♥", @"♦",@"♠",@"♣"];
}
+(NSArray *)rankStrings
{
    return @[@"?", @"A", @"2", @"3",@"4", @"5", @"6", @"7", @"8", @"9", @"10", @"J", @"Q", @"K"];
}
+(NSUInteger *) maxRank
{
    return [PlayingCard rankStrings].count -1;
}


-(void) setSuit:(NSString *)suit
{
    //utility methods is called via class name instead of an instance of the class
    if ([[PlayingCard validSuits] containsObject:suit]){
        _suit = suit;
    }
  
}

-(NSString*) suit
{
    return _suit ? _suit:@"?";
}

-(void) setRank:(NSUInteger *)rank
{
    if (rank <= [PlayingCard maxRank]){
        _rank = rank;
    }
}

@end
