//
//  Card.m
//  CardMatchingGame
//
//  Created by Yi Wang on 5/11/13.
//  Copyright (c) 2013 Yi. All rights reserved.
//

#import "Card.h"

//private declarations
@interface Card ()

@end


@implementation Card

- (int) match:(NSArray*) otherCards
{
    int score = 0;
    
    for (Card *card in otherCards){
        
        if ([card.contents isEqualToString:self.contents])
        {
            score = 1;
        }
    }
    return score;
}



@end
