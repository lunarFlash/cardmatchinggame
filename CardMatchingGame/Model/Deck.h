//
//  Deck.h
//  CardMatchingGame
//
//  Created by Yi Wang on 5/11/13.
//  Copyright (c) 2013 Yi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Card.h"

@interface Deck : NSObject

-(void) addCard : (Card *) card atTop : (BOOL) atTop;
-(Card *) drawRandomCard;
-(void) printCards;

@end
