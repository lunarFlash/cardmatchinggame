//
//  CardMatchingGame.h
//  CardMatchingGame
//
//  Created by Yi Wang on 5/12/13.
//  Copyright (c) 2013 Yi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Card.h"
#import "Deck.h"
@interface CardMatchingGame : NSObject

@property (readonly, nonatomic) int score; //readonly means theres only a getter, no setter

//designated initializer, meaning this version of init must be called for the class to work - conceptual
-(id)initWithCardCount:(NSUInteger)count
             usingDeck:(Deck *)deck;

-(void)flipCardAtIndex:(NSUInteger)index;

-(Card *)cardAtIndex:(NSUInteger)index;


@end
