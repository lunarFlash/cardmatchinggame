//
//  PlayingCardDeck.m
//  CardMatchingGame
//
//  Created by Yi Wang on 5/12/13.
//  Copyright (c) 2013 Yi. All rights reserved.
//

#import "PlayingCardDeck.h"
#import "PlayingCard.h"

@implementation PlayingCardDeck

-(id)init
{
    //sefl = super class's init to give super class to initialize it - always start with this line. id means point to an object of any/unkown class
    self = [super init];
    
    if (self) {
        for (NSString *suit in [PlayingCard validSuits]) {
            for (NSUInteger rank =1; rank <= [PlayingCard maxRank]; rank++) {
                PlayingCard *card = [[PlayingCard alloc] init];
                card.rank = rank;
                card.suit = suit;
                [self addCard:card atTop:NO];
                //NSLog(@"current card = %@", card.contents);
            }
        }
        
    }
    return self;
}





@end
