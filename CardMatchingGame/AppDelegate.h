//
//  AppDelegate.h
//  CardMatchingGame
//
//  Created by Terry Wang on 5/2/13.
//  Copyright (c) 2013 Yi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
